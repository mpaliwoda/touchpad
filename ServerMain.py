#!/usr/bin/python3.4
import socket
from MouseInfo import *
# import win32api, win32con
import sys
import time
import threading


class ServerMain(threading.Thread):#(QThread):
    def __init__(self, port, pin, parent = None):
        threading.Thread.__init__(self)
        self.stopped = False
        self.pin = pin
        self.port = port
        self.sock = None
        self.host = None
        self.connection = None

    def start_srv(self):
        try:
            self.sock = socket.socket()
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.host = socket.gethostname()
            self.sock.bind((self.host, self.port))
            self.sock.listen(1)
            self.connection, addr = self.sock.accept()
        except:
            sys.exit(3)

    def run(self):
        try:
            client_pin = None
            while client_pin != self.pin:       
                self.start_srv()
                client_pin = self.connection.recv(4)
                client_pin = client_pin.decode('utf-8')
                if client_pin != self.pin:
                    self.connection.send(bytearray('fail', 'utf-8'))
                    self.connection.close()
                    print('wrong pin ' + client_pin)
        except:
            print('Server start unsuccessful', sys.exc_info())
            sys.exit(1)

        try:
            while not self.stopped:
                arr = self.connection.recv(6)
                mouse = MouseInfo(0, 0, 0)
                mouse.x = int.from_bytes(arr[0:2], byteorder='big', signed=True)
                mouse.y = int.from_bytes(arr[2:4], byteorder='big', signed=True)
                mouse.event = int.from_bytes(arr[4:6], byteorder='big', signed=True)
                if mouse.is_stop_object():
                    break
                mouse.take_action()
                time.sleep(.0001)
        except:
            print('Connection closed', sys.exc_info())  
            self.connection.close()
            sys.exit(0)
        # else:
        #     self.sock.close()
        #     print('Pls no hackeroni sir')
        #     sys.exit(2)

pin = input("Podaj pin: \n")
srv = ServerMain(2048, pin)
srv.run()
