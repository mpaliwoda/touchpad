#!/usr/bin/python3.4
from MouseEvents import *

class MouseInfo(object):
        close_value = -32767

        move = 0
        left_click = 1
        dbl_left_click = 2
        right_click = 3
        middle_click = 4
        select_start = 5
        select_end = 6

        def __init__(self, x, y, event):
                self.x = x
                self.y = y
                self.event = event

        def to_bytes(self):
                arr = bytearray() 
                arr.extend(self.x.to_bytes(2, byteorder='big', signed=True))
                arr.extend(self.y.to_bytes(2, byteorder='big', signed=True))
                arr.extend(self.event.to_bytes(2, byteorder='big', signed=True))
                return arr
        
        def print_info(self):
                print('x: ', self.x, 
                      ' y: ', self.y,
                      ' event: ', self.event)

        def make_stop_object(self):
                self.x = MouseInfo.close_value
                self.y = MouseInfo.close_value
                self.event = MouseInfo.close_value
        
        def is_stop_object(self):
                if  self.x == MouseInfo.close_value and \
                        self.y == MouseInfo.close_value and \
                        self.event == MouseInfo.close_value:
                        return True
                return False

        def take_action(self):
            if self.event == MouseInfo.move:
                MouseEvents.move_mouse(self.x, self.y)
            elif self.event == MouseInfo.left_click:
                MouseEvents.left_click()
            elif self.event == MouseInfo.dbl_left_click:
                MouseEvents.dbl_left_click()
            elif self.event == MouseInfo.right_click:
                MouseEvents.right_click()
            elif self.event == MouseInfo.select_start:
                MouseEvents.select_start()
            elif self.event == MouseInfo.select_end:
                MouseEvents.select_end()
