#!/usr/bin/python3.4
import win32api, win32con

class MouseEvents(object):
    @staticmethod
    def move_mouse(dx, dy):
        x, y = win32api.GetCursorPos()
        if x + dx < 0:
            dx = -x
        if y + dy < 0:
            dy = -y
        win32api.SetCursorPos((x + dx, y+dy))

    @staticmethod
    def left_click():
        x, y = win32api.GetCursorPos()
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y)

    @staticmethod
    def dbl_left_click():
        MouseEvents.left_click()
        MouseEvents.left_click()

    @staticmethod
    def right_click():
        x, y = win32api.GetCursorPos()
        win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTDOWN, x, y)
        win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP, x, y)
    
    @staticmethod
    def select_start():
        x, y = win32api.GetCursorPos()
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y)

    @staticmethod
    def select_end():
        x, y = win32api.GetCursorPos()
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y)

    @staticmethod
    def middle_click():
        x, y = win32api.GetCursorPos()
        win32api.mouse_event(win32con.MOUSEEVENTF_MIDDLEDOWN, x, y)
        win32api.mouse_event(win32con.MOUSEEVENTF_MIDDLEUP, x, y)
